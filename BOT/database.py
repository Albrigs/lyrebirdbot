import sqlite3

from basics.standarts.paths import data_path


class DBHandler(object):
    def __init__(self):
        self.conn = sqlite3.connect(data_path)
        self.cursor = self.conn.cursor()

        self._create_tables(
            'music', """
id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
name TEXT NOT NULL UNIQUE,
source TEXT NOT NULL UNIQUE,
thumbnail TEXT NOT NULL UNIQUE,
tags TEXT
""")

        self._create_tables(
            'playlist', """
id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
name TEXT NOT NULL UNIQUE
""")
        self._create_tables(
            'rel_playlists_music', """
playlist_id INTEGER NOT NULL,
music_id INTEGER NOT NULL,
FOREIGN KEY (playlist_id) REFERENCES playlist(id),
FOREIGN KEY (music_id) REFERENCES music(id)
""")
        self._create_tables(
            'tag', """
id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
name TEXT NOT NULL UNIQUE,
color TEXT NOT NULL UNIQUE
""")

    def close_db(self):
        self.conn.close()

    def _create_tables(self, name, definition):
        self.cursor.execute(f"""CREATE TABLE IF NOT EXISTS {name} (
{definition}
);
        """)
