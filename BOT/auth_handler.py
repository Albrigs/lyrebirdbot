from os import environ

from flask import Flask, g, jsonify, redirect, request, session, url_for
from requests_oauthlib import OAuth2Session

from basics.cmd_parse import CmdParser
from basics.standarts.paths import user_file
from basics.standarts.secret import CLIENT_ID, CLIENT_SECRET
from basics.standarts.urls import (API_BASE_URL, AUTH_BASE_URL, BASE_URL,
                                   REDIRECT_URI, TOKEN_URL)
from basics.yaml import write_yaml_file

if 'http://' in REDIRECT_URI:
    environ['OAUTHLIB_INSECURE_TRANSPORT'] = 'true'

api = Flask(__name__)
api.debug = False
api.config['SECRET_KEY'] = CLIENT_SECRET


def token_updater(token):
    session['oauth2_token'] = token


def make_session(token=None, state=None, scope=None):
    return OAuth2Session(client_id=CLIENT_ID,
                         token=token,
                         state=state,
                         scope=scope,
                         redirect_uri=REDIRECT_URI,
                         auto_refresh_kwargs={
                             'client_id': CLIENT_ID,
                             'client_secret': CLIENT_SECRET,
                         },
                         auto_refresh_url=TOKEN_URL,
                         token_updater=token_updater)


@api.route('/')
def index():
    scope = request.args.get('scope',
                             'identify email connections guilds guilds.join')
    discord = make_session(scope=scope.split(' '))
    authorization_url, state = discord.authorization_url(AUTH_BASE_URL)
    session['oauth2_state'] = state
    return redirect(authorization_url)


@api.route('/callback')
def callback():
    if request.values.get('error'):
        return request.values['error']
    discord = make_session(state=session.get('oauth2_state'))
    token = discord.fetch_token(TOKEN_URL,
                                client_secret=CLIENT_SECRET,
                                authorization_response=request.url)
    session['oauth2_token'] = token
    return redirect(url_for('.me'))


@api.route('/me')
def me():
    discord = make_session(token=session.get('oauth2_token'))
    user = discord.get(API_BASE_URL + '/users/@me').json()
    guilds = discord.get(API_BASE_URL + '/users/@me/guilds').json()
    connections = discord.get(API_BASE_URL + '/users/@me/connections').json()
    res = {'user': user, 'guilds': guilds, 'connections': connections}
    write_yaml_file(user_file, dict(res))
    CmdParser().store_user_info(res)
    return """
<html lang="pt-br">
<head><title>Autorizado</title></head>
<body>
    <h1>OK</h1>
</body>
</html>
    """


def start_api():
    api.run(port=5000)
