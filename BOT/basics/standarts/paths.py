from os.path import expanduser, join

base_path = expanduser('~/.lyrebird')
base_file = join(base_path, 'communication.yaml')
state_file = join(base_path, 'state.yaml')
user_file = join(base_path, 'user.yaml')
music_dir = join(base_path, 'music')
image_dir = join(base_path, 'img')
data_path = join(base_path, 'data.db')
base_nothing = dict(do='nothing')
