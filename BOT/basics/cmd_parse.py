from .standarts.cmds import cmd_standarts
from .standarts.paths import base_file
from .yaml import write_yaml_file


class CmdParser(object):
    def __send_comand(self, cmd, content=0):
        write_yaml_file(base_file, dict({'do': cmd, 'content': content}))

    def store_user_info(self, user_info):
        self.__send_comand(cmd_standarts.STORE_USER, user_info)
