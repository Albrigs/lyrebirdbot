from yaml import dump as yaml_dump
from yaml import safe_load as load_yaml


def write_yaml_file(path, content):
    file = open(path, 'w')
    yaml_dump(content, file, default_flow_style=False)
    file.close()


def read_yaml(path):
    base_input = open(path, 'r')
    res = load_yaml(base_input)
    base_input.close()
    return res
