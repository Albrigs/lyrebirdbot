from os import makedirs
from os.path import isdir, isfile

from basics.standarts.paths import (base_file, base_nothing, image_dir,
                                    music_dir, state_file)
from basics.yaml import write_yaml_file


def reset_base_file():
    write_yaml_file(base_file, base_nothing)


def gen_base_env():
    if not isdir(music_dir):
        makedirs(music_dir)
    if not isdir(image_dir):
        makedirs(image_dir)
    if not isfile(base_file):
        reset_base_file()
    if not isfile(state_file):
        write_yaml_file(state_file, base_nothing)
