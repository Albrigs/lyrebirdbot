import webbrowser
from multiprocessing import Process

from auth_handler import start_api
from base_screen import MainApp
from basics.cmd_parse import CmdParser
from basics.standarts.urls import BASE_URL
from database import DBHandler
from discord_bot import start_bot
from env_config import gen_base_env


class AuthController(object):
    def start(self, wdg=0):
        self.browser = webbrowser.get()
        self.api_thread = Process(target=start_api)
        self.api_thread.start()
        self.browser.open(BASE_URL)

    def stop(self, wdg=0):
        self.api_thread.terminate()


gen_base_env()

gui = MainApp(db=DBHandler(), auth_controller=AuthController())

gui_thread = Process(target=gui.run)
gui_thread.start()

start_bot()
