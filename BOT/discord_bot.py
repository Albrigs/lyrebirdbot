from os.path import isfile

import discord
from discord.ext import commands, tasks

from basics.standarts.cmds import cmd_standarts
from basics.standarts.paths import base_nothing
from basics.standarts.secret import TOKEN
from basics.yaml import read_yaml
from env_config import base_file, reset_base_file


class BotUser(object):
    def __init__(self, user_info):
        self.username = user_info['user']['username']
        self.user_id = user_info['user']['id']
        self.guilds = user_info['guilds']
        self.guilds = [{'name': e['name'], 'id': e['id']} for e in self.guilds]


bot = commands.Bot(command_prefix='¬')
bot.cur_user = 0


async def get_user_by_id(id: int):
    # Se estiver em cache
    user = bot.get_user(id)
    # Se não estiver em cache
    if user is None:
        user = await bot.fetch_user(id)
    return user


@bot.event
async def on_ready():
    print(f'Logado como {bot.user}')


@tasks.loop(seconds=1)
async def do_on_loop():
    bot_input = read_yaml(base_file)
    if base_nothing != bot_input:
        # TODO Inserir comandos aqui
        if bot_input['do'] == cmd_standarts.STORE_USER:
            bot.cur_user = BotUser(bot_input['content'])
    # TODO Fim da Zona de comandos

        reset_base_file()


def start_bot():
    do_on_loop.start()
    bot.run(TOKEN)
