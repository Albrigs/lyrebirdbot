from os.path import isfile

from kivy.app import App
from kivy.clock import Clock
from kivy.config import Config
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen, ScreenManager
from screeninfo import get_monitors

from basics.standarts.paths import user_file
from basics.yaml import read_yaml

monitor = get_monitors()[0]
width = int(monitor.width / 4)
height = monitor.height - 80

Config.set('graphics', 'width', width)
Config.set('graphics', 'height', height)
Config.set('graphics', 'resizable', False)
Config.set('graphics', 'position', 'custom')
Config.set('graphics', 'left', 10)
Config.set('graphics', 'top', 20)


class NotAuthScreen(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        layout = BoxLayout(orientation='vertical')
        layout.add_widget(
            Label(text='Nenhum USUÁRIO LOCALIZADO, por favor faça login'))
        self.add_widget(layout)


class ConfirmAuthScreen(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        app = App.get_running_app()

        layout = BoxLayout(orientation='vertical')
        user_info = read_yaml(user_file)
        user_label = Label(
            text=f"Logado como:\n{user_info['user']['username']}")
        servers_text = 'Presente nesses servidores:'
        for e in user_info['guilds']:
            servers_text = f"{servers_text}\n{e['name']}"

        servers_label = Label(text=servers_text)

        lable_div = BoxLayout(orientation='horizontal')
        lable_div.add_widget(user_label)
        lable_div.add_widget(servers_label)

        button_ok = Button(text='Confirmar')
        button_nok = Button(text='Mudar usuário\nOU\nAtualizar',
                            on_release=app.auth_controller.start)
        buttons_div = BoxLayout(orientation='horizontal')
        buttons_div.add_widget(button_ok)
        buttons_div.add_widget(button_nok)

        layout.add_widget(lable_div)
        layout.add_widget(buttons_div)

        self.add_widget(layout)


class MainApp(App):
    def __init__(self, db, auth_controller, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.db = db
        self.auth_controller = auth_controller

    def auth_checker(self, dt):
        print('checando')
        if isfile(user_file):
            print('autenticado')

    def build(self):
        screen_manager = ScreenManager()

        if isfile(user_file):
            screen_manager.add_widget(ConfirmAuthScreen())

        else:
            screen_manager.add_widget(NotAuthScreen())
            Clock.schedule_once(self.auth_checker, 1)
            self.auth_controller.start()

        return screen_manager
